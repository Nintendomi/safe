import React from "react";
import logo from "../logo.png";
import LogOutIco from "../assets/icone/logout.png";
import HelpIco from "../assets/icone/help.png";
import ScreenIco from "../assets/icone/screen.png";
import HseIco from "../assets/icone/hse.png";
import PrintIco from "../assets/icone/print.png";

function Header({ azienda, ente, setState, state }) {
  const logOut = () => {
    setState({
      azienda: null,
      ente: null,
      posizione: ["Lista Aziende"],
      logged: false,
    });
  };

  return (
    <header>
      <div className="menu">Menu</div>
      <div className="azienda">
        {azienda}
        {ente !== null && " | " + ente}
      </div>
      <div className="logo">
        <img src={logo} alt="SAFE" />
      </div>
      <div className="icone">
        <img title="Stampa" src={PrintIco} alt="Stampa" />
        <img title="Screenshot" src={ScreenIco} alt="Screenshot" />
        <img title="Vai a HSE" src={HseIco} alt="Vai a HSE" />
        <img title="Aiuto" src={HelpIco} alt="Aiuto" />
        <img title="Esci" onClick={logOut} src={LogOutIco} alt="Esci" />
      </div>
    </header>
  );
}

export default Header;
