import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

function Breadcrumb({ posizione }) {
  return (
    <div id="breadcrumb">
      <FontAwesomeIcon icon="home" style={{ marginRight: "5px" }} />
      {posizione.map((voce, index) => (
        <span key={index}>
          {index !== 0 ? <span className="slash"> / </span> : ""}
          <span>{voce}</span>
        </span>
      ))}
    </div>
  );
}

export default Breadcrumb;
