import React, { useState } from "react";
import Header from "../components/Header";
import Breadcrumb from "../components/Breadcrumb";
import Json from "../jsons";
import logoAutostradeItalia from "../assets/loghi/autostrade-italia.png";

const listaAziende = Json("listaAziende");

function VoceAzienda({ azienda, ente, setState, state, logo }) {
  const classes =
    state.ente === ente && state.azienda === azienda
      ? "azienda selected"
      : "azienda";

  const handleClick = () => {
    // inserisce l'azienda selezionata nello state
    setState({ ...state, azienda: azienda, ente: ente });
  };

  const getLogo = (name) => {
    if (name === "autostrade-italia")
      return { backgroundImage: "url(" + logoAutostradeItalia + ")" };
    //if (name === "essediesse")
    //return { backgroundImage: "url(" + logoEssediesse + ")" };
  };

  return (
    <div onClick={handleClick} className={classes}>
      <div className="logo" style={getLogo(logo)} />
      <div className="divisore" />
      {azienda}
      <br />
      {ente}
    </div>
  );
}

function ListaAziende({ state, setState, categoria }) {
  return (
    <div id="listaAziende">
      {listaAziende[categoria].map((voce) => (
        <VoceAzienda
          key={voce.ente}
          state={state}
          setState={setState}
          ente={voce.ente}
          azienda={voce.azienda}
          logo={voce.logo}
        />
      ))}
    </div>
  );
}

function VoceMenu({ children, selected, setCategoria }) {
  const selectedColor = { backgroundColor: "var(--blue)", color: "white" };
  const normalColor = { backgroundColor: "var(--grey)" };
  return (
    <div
      className="voceMenu"
      onClick={setCategoria}
      style={selected ? selectedColor : normalColor}
    >
      {children}
    </div>
  );
}

function MenuAziende({ categoria, setCategoria }) {
  const categorie = [
    { key: "autostrade-italia", nome: "Aziende Autostrade per l'Italia" },
    { key: "autostrade-tech", nome: "Aziende Autostrade Tech S.p.A." },
    { key: "autostrade-service", nome: "Aziende Autostrade Service" },
    { key: "essediesse", nome: "Aziende Essediesse" },
    { key: "towerco", nome: "Aziende TowerCo S.p.A." },
    { key: "telepass", nome: "Aziende Telepass S.p.A." },
    { key: "altre", nome: "Altre Aziende" },
  ];

  return (
    <div id="menuAziende">
      {categorie.map((cat) => (
        <VoceMenu
          key={cat.key}
          setCategoria={() => setCategoria(cat.key)}
          selected={categoria === cat.key ? true : false}
        >
          {cat.nome}
        </VoceMenu>
      ))}
    </div>
  );
}

function Home({ state, setState }) {
  const [catSelez, setCatSelez] = useState("autostrade-italia");

  return (
    <>
      <Header
        azienda={state.azienda}
        ente={state.ente}
        setState={setState}
        state={state}
      />
      <Breadcrumb posizione={state.posizione} />
      <div id="home">
        <MenuAziende setCategoria={setCatSelez} categoria={catSelez} />
        <ListaAziende categoria={catSelez} state={state} setState={setState} />
      </div>
    </>
  );
}

export default Home;
