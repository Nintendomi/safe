import React, { useState } from "react";
import logo from "../logo.png";
import Json from "../jsons";

const dati = Json("accesso");

function Intro(props) {
  const gruppo1 = dati.gruppo1.map((membro) => (
    <span key={membro}>
      {membro}
      <br />
    </span>
  ));
  const gruppo2 = dati.gruppo2.map((membro) => (
    <span key={membro}>
      {membro}
      <br />
    </span>
  ));

  return (
    <div id="intro">
      <div className="title">
        <h4>Direzione Centrale Risorse</h4>
        <h2>
          <b>H</b>ealth, <b>S</b>afety & <b>E</b>nvironment
        </h2>
        <h3>Salute, Sicurezza e Ambiente</h3>
      </div>
      <div className="mission">
        <h5>LA MISSION</h5>
        <p>{dati.mission}</p>
      </div>
      <div className="gruppo">
        <h5>IL GRUPPO</h5>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div style={{ flex: 1 }}>
            <span style={{ fontWeight: "700" }}>{dati.responsabile} </span>
            <br />
            (Responsabile HSE)
            <br />
            <br />
            {gruppo1}
          </div>
          <div style={{ flex: 3 }}>{gruppo2}</div>
        </div>
      </div>
    </div>
  );
}

function Login({ changePage, state, setState }) {
  const [pass, setPass] = useState("");
  const [user, setUser] = useState("");

  const submitLogin = (e) => {
    e.preventDefault();
    if (user === "admin" && pass === "admin") {
      setState({ ...state, logged: true });
      setPass("");
      setUser("");
    } else
      alert(
        "Il nome Utente o la Password inseriti non sono corretti. Riprova!"
      );
  };

  return (
    <div id="login">
      <div id="content">
        <img src={logo} alt="SAFE" />
        <h3>Autenticazione</h3>
        <form onSubmit={submitLogin}>
          <label>Utente</label>
          <input
            type="text"
            className="form-control mb-3"
            onChange={(e) => setUser(e.target.value)}
            placeholder="Username"
            value={user}
          />
          <label>Password</label>
          <input
            type="password"
            className="form-control mb-4"
            onChange={(e) => setPass(e.target.value)}
            placeholder="Password"
            value={pass}
          />
          <div></div>
          <input
            type="submit"
            value="Accedi"
            className="btn btn-primary col-6"
          />
        </form>
      </div>
      <div id="developer"></div>
    </div>
  );
}

function Accesso({ setState, state }) {
  return (
    <div id="accesso">
      <Intro />
      <Login setState={setState} state={state} />
    </div>
  );
}

export default Accesso;
