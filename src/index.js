import React, { useState } from "react";
import ReactDOM from "react-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Accesso from "./pages/Accesso";
import Home from "./pages/Home";

//icone di FontAwesome
library.add(fas, far);

function App(props) {
  const pages = {
    home: Home,
    accesso: Accesso,
  };
  const [page, setPage] = useState("home");
  const [state, setState] = useState({
    azienda: null,
    ente: null,
    posizione: ["Lista Aziende"],
    logged: true, // mettere a false!!!
  });
  const ActualPage = pages[page];
  if (state.logged) {
    return (
      <ActualPage changePage={setPage} state={state} setState={setState} />
    );
  } else {
    return <Accesso state={state} setState={setState} />;
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
