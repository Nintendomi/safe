import accesso from "../jsons/accesso.json";
import listaAziende from "../jsons/listaAziende.json";

export default function Json(tipologia) {
  const jsons = {
    accesso: accesso,
    listaAziende: listaAziende,
  };
  return jsons[tipologia];
}
